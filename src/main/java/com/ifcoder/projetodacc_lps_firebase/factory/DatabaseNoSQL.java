/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ifcoder.projetodacc_lps_firebase.factory;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.Firestore;
import com.google.firebase.cloud.FirestoreClient;
import java.io.FileInputStream;
import java.io.IOException;

/**
 *
 * @author jose
 */
public class DatabaseNoSQL {

    public DatabaseNoSQL() {
        conect();
    }
            
    public void conect() {                
         try {
            FileInputStream serviceAccount = new FileInputStream("projetodacc-lps-firebase-adminsdk-eg5y3-5073470f65.json");
            FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .build();
            if (FirebaseApp.getApps().isEmpty()) {
                FirebaseApp.initializeApp(options);
            }            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public Firestore getDataBase(){
        return FirestoreClient.getFirestore();
    }
    
    public CollectionReference getCollection(String nomeColecao){
        Firestore db = getDataBase();
        return db.collection(nomeColecao);
    }

}
