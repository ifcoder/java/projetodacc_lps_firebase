/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ifcoder.projetodacc_lps_firebase.model.dao;

import com.google.api.core.ApiFuture;
import com.ifcoder.projetodacc_lps_firebase.model.Aluno;

import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.WriteResult;
import com.ifcoder.projetodacc_lps_firebase.factory.DatabaseNoSQL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AlunoDAO implements IDao {

    private DatabaseNoSQL bancoFirebase;
    private CollectionReference alunos;

    public AlunoDAO() {
        bancoFirebase = new DatabaseNoSQL();
        alunos = bancoFirebase.getCollection("alunos");
    }

    @Override
    public void save(Object obj) {
        try {
            Aluno aluno = (Aluno) obj;
            Map<String, Object> data = new HashMap<>();
            data.put("nome", aluno.getNome());
            data.put("sexo", String.valueOf(aluno.getSexo()));
            data.put("idade", aluno.getIdade());
            data.put("matricula", aluno.getMatricula());
            data.put("anoIngresso", aluno.getAnoIngresso());
            ApiFuture<WriteResult> future = alunos.document(aluno.getMatricula()).set(data);
            //ApiFuture<WriteResult> future = alunos.document().set(data); //assim o FB cria um idautomatico para a documento.
            WriteResult result = future.get();
        } catch (InterruptedException ex) {
            Logger.getLogger(AlunoDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ExecutionException ex) {
            Logger.getLogger(AlunoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void update(Object obj) {
        try {
            Aluno aluno = (Aluno) obj;
            DocumentReference docRef = alunos.document(aluno.getMatricula());
            Map<String, Object> updates = new HashMap<>();
            updates.put("nome", aluno.getNome());
            updates.put("sexo", String.valueOf(aluno.getSexo()));
            updates.put("idade", aluno.getIdade());
            updates.put("matricula", aluno.getMatricula());
            updates.put("anoIngresso", aluno.getAnoIngresso());
            
            ApiFuture<WriteResult> future = docRef.update(updates);
            WriteResult result = future.get();
        } catch (InterruptedException ex) {
            Logger.getLogger(AlunoDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ExecutionException ex) {
            Logger.getLogger(AlunoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Object> findAll() {
        List<Object> list = new ArrayList<>();
        try {
            QuerySnapshot querySnapshot = alunos.get().get();
            for (QueryDocumentSnapshot doc : querySnapshot) {
                Object obj = doc.get("idade");
                int idade = ((Long)obj).intValue();
                obj = doc.get("anoIngresso");
                int ano = ((Long)obj).intValue();
                Aluno aluno = new Aluno(
                        1,
                        doc.getString("nome"),
                        doc.getString("sexo").charAt(0),
                        idade,
                        doc.getString("matricula"),
                        ano);
                list.add(aluno);
            }
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public Object find(Object obj) {
        Aluno aluno = (Aluno) obj;
        DocumentReference docRef = alunos.document(aluno.getMatricula());
        Aluno a = null;
        try {
            a = docRef.get().get().toObject(Aluno.class);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return a;
    }

    public Object findByMatricula(String matricula) {
        DocumentReference docRef = alunos.document(matricula);
        Aluno aluno = null;
        try {
            aluno = docRef.get().get().toObject(Aluno.class);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return aluno;
    }

    @Override
    public boolean delete(Object obj) {
        try {
            Aluno aluno = (Aluno) obj;
            DocumentReference docRef = alunos.document(aluno.getMatricula());
            ApiFuture<WriteResult> future = docRef.delete();
            WriteResult result = future.get();
            return true;
        } catch (InterruptedException ex) {
            Logger.getLogger(AlunoDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ExecutionException ex) {
            Logger.getLogger(AlunoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
}

