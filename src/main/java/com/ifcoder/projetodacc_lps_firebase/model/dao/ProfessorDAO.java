package com.ifcoder.projetodacc_lps_firebase.model.dao;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.*;
import com.ifcoder.projetodacc_lps_firebase.factory.DatabaseNoSQL;
import com.ifcoder.projetodacc_lps_firebase.model.Professor;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProfessorDAO implements IDao {

    private DatabaseNoSQL bancoFirebase;
    private CollectionReference professores;

    public ProfessorDAO() {
        bancoFirebase = new DatabaseNoSQL();
        professores = bancoFirebase.getCollection("professores");
    }

    @Override
    public void save(Object obj) {
        try {
            Professor professor = (Professor) obj;
            Map<String, Object> data = new HashMap<>();
            data.put("nome", professor.getNome());
            data.put("sexo", String.valueOf(professor.getSexo()));
            data.put("idade", professor.getIdade());
            data.put("cpf", professor.getCpf());
            // ... outros campos

            ApiFuture<DocumentReference> future = professores.add(data);
            DocumentReference docRef = future.get();
        } catch (InterruptedException | ExecutionException e) {
            Logger.getLogger(ProfessorDAO.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void update(String docId, Professor updatedProfessor) {
        try {
            DocumentReference docRef = professores.document(docId);
            Map<String, Object> updates = new HashMap<>();
            updates.put("nome", updatedProfessor.getNome());
            updates.put("sexo", String.valueOf(updatedProfessor.getSexo()));
            updates.put("idade", updatedProfessor.getIdade());
            updates.put("cpf", updatedProfessor.getCpf());

            ApiFuture<WriteResult> future = docRef.update(updates);
            WriteResult result = future.get();
        } catch (InterruptedException | ExecutionException e) {
            Logger.getLogger(ProfessorDAO.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    @Override
    public List<Object> findAll() {
        List<Object> list = new ArrayList<>();
        try {
            QuerySnapshot querySnapshot = professores.get().get();
            for (QueryDocumentSnapshot doc : querySnapshot) {
                Object obj = doc.get("idade");
                int idade = ((Long) obj).intValue();
                String nome = doc.getString("nome");
                char sexo = doc.getString("sexo").charAt(0);
                String cpf = doc.getString("cpf");

                Professor professor = new Professor(
                        nome,
                        sexo,
                        idade,
                        cpf
                );

                list.add(professor);
            }
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public Object find(Object obj) {
        Professor professor = (Professor) obj;
        DocumentReference docRef = professores.document(professor.getCpf()); // Aqui assumo que você ainda quer procurar pelo CPF
        Professor p = null;
        try {
            p = docRef.get().get().toObject(Professor.class);
        } catch (ExecutionException | InterruptedException e) {
            Logger.getLogger(ProfessorDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return p;
    }

    @Override
    public boolean delete(Object obj) {
        try {
            Professor professor = (Professor) obj;

            // Encontrar documentos que possuem o mesmo CPF
            ApiFuture<QuerySnapshot> future = professores.whereEqualTo("cpf", professor.getCpf()).get();
            List<QueryDocumentSnapshot> documents = future.get().getDocuments();

            // Se um ou mais documentos são encontrados, pegar o primeiro e deletá-lo
            if (!documents.isEmpty()) {
                QueryDocumentSnapshot document = documents.get(0);

                // Deletar o documento usando seu ID
                ApiFuture<WriteResult> deleteFuture = professores.document(document.getId()).delete();
                WriteResult deleteResult = deleteFuture.get();

                return true;
            }
        } catch (InterruptedException | ExecutionException e) {
            Logger.getLogger(ProfessorDAO.class.getName()).log(Level.SEVERE, null, e);
        }

        return false;
    }

    public Professor findByCpf(String cpf) {
        Professor professor = null;

        try {
            Query query = professores.whereEqualTo("cpf", cpf);
            ApiFuture<QuerySnapshot> querySnapshot = query.get();

            List<QueryDocumentSnapshot> documents = querySnapshot.get().getDocuments();

            if (documents.size() > 0) {
                QueryDocumentSnapshot document = documents.get(0);
                professor = document.toObject(Professor.class);
            }

        } catch (InterruptedException | ExecutionException e) {
            Logger.getLogger(ProfessorDAO.class.getName()).log(Level.SEVERE, null, e);
        }

        return professor;
    }
}
