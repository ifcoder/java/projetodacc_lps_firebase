/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ifcoder.projetodacc_lps_firebase.controller;

import com.ifcoder.projetodacc_lps_firebase.model.Professor;
import com.ifcoder.projetodacc_lps_firebase.model.dao.ProfessorDAO;
import com.ifcoder.projetodacc_lps_firebase.model.valid.ValidateProfessor;
import com.ifcoder.projetodacc_lps_firebase.model.exceptions.ProfessorException;
import java.util.List;
import javax.swing.JTable;

/**
 *
 * @author jose
 */
public class ProfessorController {

    private ProfessorDAO repositorio;

    public ProfessorController() {
        repositorio = new ProfessorDAO();
    }

    public void cadastrarProfessor(String nome, String sexo, String idade, String cpf) {
        ValidateProfessor valid = new ValidateProfessor();
        Professor novoProfessor = valid.validacao(nome, sexo, idade, cpf);
        

        if (repositorio.findByCpf(cpf) == null) {
            repositorio.save(novoProfessor);
        } else {
            throw new ProfessorException("Error - Já existe um professor com este 'CPF'.");
        }
    }

    public void atualizarProfessor(String cpfOriginal, String nome, String sexo, String idade, String cpf) {
        ValidateProfessor valid = new ValidateProfessor();
        Professor novoProfessor = valid.validacao(nome, sexo, idade, cpf);
        
        repositorio.update(cpfOriginal, novoProfessor);
    }

    public Professor buscarProfessor(String cpf) {
        return this.repositorio.findByCpf(cpf);
    }

    public void excluirProfessor(Professor prof) {
        if(prof != null){
            repositorio.delete(prof);                    
        } else {
            throw new ProfessorException("Error - Professor inexistente.");
        }        
    }

    public void atualizarTabela(JTable grd) {
        List<Object> lst = repositorio.findAll();
        
        TMCadProfessor tmProfessor = new TMCadProfessor(lst);
        grd.setModel(tmProfessor);        
    }
    
    

}
