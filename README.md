# ProjetoDacc - LPS uso do FIREBASE - Banco NoSQL da google

Este projeto é uma continuação da disciplina de Laboratório de Programação de Sistemas (LPS) e é uma aplicação Java Swing que interage com um BaaS (Backend as a service), o Firebase-Firestore da google.

![Abordagen](./src/main/resources/imagens/assets/api-firebase.png)

## Sobre o Projeto

A aplicação permite que os usuários realizem operações CRUD (Criar, Ler, Atualizar e Deletar) para gerenciar informações de professores. A interface gráfica foi desenvolvida usando Java Swing, e a comunicação com o banco de dados é feita através de arquivo de configuração gerado no firebase.

### Maven

- Por ser um projeto Maven (gerenciador de dependências atual), fique atento a algumas configurações no arquivo POM.xml.
- Todas as dependências que seu projeto necessitarão serão colocadas neste arquivo XML.
- Caso necessite de alguma outra, busque no repositório central [Maven Central](https://search.maven.org/) ou [MVN Repository](https://mvnrepository.com/).

### Estrutura do Projeto

- O cadastro de Professor se comunica com o Firebase.
- O cadastro de Aluno se comunica com o Firebase.
- O cadastro de Funcionário utiliza JPA-Hibernate com banco de dados MySQL. 


## Firebase (BaaS)
- Estou usando a conta teste do professor: teste.dacc.ifet@gmail.com


## Instalação e Execução

1. Clone o repositório:
   ```sh
   git clone https://github.com/seu_usuario/seu_repositorio.git
   ```

2. Navegue até a pasta do projeto e execute o arquivo JAR (certifique-se de ter o Java instalado):
   ```sh
   java -jar nome_do_arquivo.jar
   ```

### Integração com API-Firebase
- A ideia aqui é criar um front end atraves do java swing
- Depois, este mesmo backend firebase poderá servir a outros fronts (web-mobile)

## Screenshots

### Home
<p align="center">
  <img src="./src/main/resources/imagens/assets/Home_LPS_API.png" alt="home" width="250"/>
</p>

### Cadastro de Professor e Aluno via Firebase - Firestore

![Abordagen](./src/main/resources/imagens/assets/api-firebase.png)
  
